'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

require('events');
require('uuid/v4');
require('node-schedule');
require('./chunks/af9236c0.js');
require('koa');
require('koa-router');
var commander = _interopDefault(require('commander'));
var __chunk_2 = require('./chunks/3f7cb291.js');
require('pino');

var version = "0.0.1";

commander.version(version);
commander.command('serve')
    .description('start web service for scheduling')
    .option('-t, --trace', 'enable tracing events')
    .option('-f, --file', 'file to log in')
    .action(__chunk_2.serve);
commander.command('set')
    .description('schedule task')
    .option('-t, --timeout <timeout>', 'set timeout in ms', parseInt)
    .action(() => { });
commander.parse(process.argv);
//# sourceMappingURL=cli.js.map
