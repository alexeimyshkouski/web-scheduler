'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

require('events');
require('uuid/v4');
require('node-schedule');
var __chunk_1 = require('./chunks/af9236c0.js');
var Koa = _interopDefault(require('koa'));
var KoaRouter = _interopDefault(require('koa-router'));

const scheduler = new __chunk_1.Scheduler();
const router = new KoaRouter();
router
    .get('/schedule', ctx => {
    ctx.body = scheduler.schedule({
        timeout: ctx.query.timeout ? parseInt(ctx.query.timeout) : undefined,
        date: ctx.query.timeout ? parseInt(ctx.query.timeout) : undefined
    });
})
    .get('/cancel', ctx => {
    const cancelled = scheduler.cancel(ctx.query.id);
    ctx.status = cancelled ? 200 : 400;
});
const server = new Koa();
server
    .use(router.middleware())
    .use(router.allowedMethods());
//# sourceMappingURL=app.js.map

exports.scheduler = scheduler;
exports.server = server;
