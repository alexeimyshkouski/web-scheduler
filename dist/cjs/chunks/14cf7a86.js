'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var __chunk_1 = require('./af9236c0.js');
var Koa = _interopDefault(require('koa'));
var KoaRouter = _interopDefault(require('koa-router'));
var Pino = _interopDefault(require('pino'));

const scheduler = new __chunk_1.Scheduler();
const router = new KoaRouter();
router
    .get('/schedule', ctx => {
    ctx.body = scheduler.schedule({
        timeout: ctx.query.timeout ? parseInt(ctx.query.timeout) : undefined,
        date: ctx.query.timeout ? parseInt(ctx.query.timeout) : undefined
    });
})
    .get('/cancel', ctx => {
    const cancelled = scheduler.cancel(ctx.query.id);
    ctx.status = cancelled ? 200 : 400;
});
const server = new Koa();
server
    .use(router.middleware())
    .use(router.allowedMethods());
//# sourceMappingURL=app.js.map

function serve(options = {}) {
    options = Object.assign({
        port: 5005,
        trace: false,
        file: null
    }, options);
    let dest, opts = {};
    if (options.file) {
        dest = Pino.extreme(options.file);
    }
    else {
        opts = {
            prettyPrint: {
                levelFirst: true,
                translateTime: 'SYS:yyyy-mm-dd HH:MM:ss.l'
            }
        };
        dest = Pino.destination();
    }
    const logger = Pino(opts, dest);
    if (options.trace) {
        logger.level = 'trace';
    }
    function traceEvent(event, id) {
        logger.trace({ id }, event);
    }
    for (let event of ['schedule', 'expire', 'cancel']) {
        scheduler.on(event, traceEvent.bind(scheduler, event));
    }
    server.listen(options.port, () => {
        logger && logger.info({ port: options.port }, 'listening');
    });
}

exports.serve = serve;
