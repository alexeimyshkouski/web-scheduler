'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var EventEmitter = _interopDefault(require('events'));
var uuid4 = _interopDefault(require('uuid/v4'));
var schedule = _interopDefault(require('node-schedule'));

// internals
class ImmediateJob {
    constructor(cb) {
        this.date = new Date();
        this.invoked = false;
        this.cancelled = false;
        this.immediateID = setImmediate(() => {
            this.invoked = true;
            cb();
        });
    }
    nextInvocation() {
        return this.date;
    }
    cancel() {
        if (this.invoked) {
            return false;
        }
        clearImmediate(this.immediateID);
        return this.cancelled = true;
    }
}
function isNumber(value) {
    return (typeof value)[0] === 'n';
}
function _emit(ID) {
    this._scheduled.delete(ID);
    setImmediate(this.emit.bind(this, 'expire', ID));
}
class Scheduler extends EventEmitter {
    constructor() {
        super();
        this._scheduled = new Map();
    }
    get(ID) {
        return this._scheduled.get(ID);
    }
    has(ID) {
        return this._scheduled.has(ID);
    }
    schedule(options = {}, payload = '') {
        const ID = uuid4();
        let scheduleOption = null;
        if (isNumber(options.timeout) && options.timeout > 0) {
            scheduleOption = new Date(Date.now() + options.timeout);
        }
        else if (isNumber(options.date)) {
            scheduleOption = new Date(options.date);
        }
        let job;
        const cb = _emit.bind(this, ID);
        let emit;
        if (scheduleOption) {
            job = schedule.scheduleJob(scheduleOption, cb);
        }
        else {
            job = new ImmediateJob(cb);
        }
        if (job) {
            this._scheduled.set(ID, job);
            emit = this.emit.bind(this, 'schedule', ID);
        }
        else {
            emit = this.emit.bind(this, 'cancel', ID);
        }
        setImmediate(emit);
        return ID;
    }
    cancel(ID) {
        if (this._scheduled.has(ID)) {
            const job = this._scheduled.get(ID);
            this._scheduled.delete(ID);
            const cancelled = job.cancel();
            if (cancelled) {
                setImmediate(this.emit.bind(this, 'cancel', ID));
                return cancelled;
            }
        }
        return false;
    }
}

exports.Scheduler = Scheduler;
