import 'events';
import 'uuid/v4';
import 'node-schedule';
import { S as Scheduler } from './chunks/6651178a.mjs';
import Koa from 'koa';
import KoaRouter from 'koa-router';

const scheduler = new Scheduler();
const router = new KoaRouter();
router
    .get('/schedule', ctx => {
    ctx.body = scheduler.schedule({
        timeout: ctx.query.timeout ? parseInt(ctx.query.timeout) : undefined,
        date: ctx.query.timeout ? parseInt(ctx.query.timeout) : undefined
    });
})
    .get('/cancel', ctx => {
    const cancelled = scheduler.cancel(ctx.query.id);
    ctx.status = cancelled ? 200 : 400;
});
const server = new Koa();
server
    .use(router.middleware())
    .use(router.allowedMethods());

export { scheduler, server };
