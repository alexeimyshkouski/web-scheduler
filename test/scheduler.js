const assert = require('assert')

const Scheduler = require('../').default

describe('Instance Methods', () => {
    describe('#schedule()', () => {
        const scheduler = new Scheduler()

        const timeout = 1
        it(`${ timeout }ms timeout`, function () {
            this.timeout(timeout * 1.1 + 5)
            const timer = process.hrtime()

            const ID = scheduler.schedule({
                timeout
            })

            scheduler.once('expire', expiredID => {
                const diff = process.hrtime(timer)
                assert.equal(ID, expiredID)
                const metrics = `${ diff[0] * 1e3 + Number((diff[1] / 1e6).toFixed(2)) }ms`
                addContext(this, { metrics })
                console.log(metrics)
            })
        })
    })
})

describe('Scenarios', () => {
    describe('Emitting events', () => {
        let scheduler = new Scheduler()
        const timeout = 10

        it(`schedule`, function () {
            this.timeout(timeout * 1.1 + 5)
            return new Promise(resolve => {
                const ID = scheduler.schedule({
                    timeout
                })

                scheduler.on('schedule', () => {
                    resolve()
                })
            })
        })
        
        it(`expire`, function () {
            this.timeout(timeout * 1.1 + 5)
            const ID = scheduler.schedule({
                timeout
            })

            scheduler.once('expire', expiredID => {
                assert.equal(ID, expiredID)
            })
        })

        it(`cancel`, function () {
            this.timeout(timeout * 1.1 + 5)
            const ID = scheduler.schedule({
                timeout
            })

            scheduler.once('cancel', cancelledID => {
                assert.equal(ID, cancelledID)
            })

            scheduler.cancel(ID)
        })
    })
})
