FROM node:alpine

ARG WORKDIR=/home/node/
WORKDIR $WORKDIR

ADD dist/cjs $WORKDIR
ADD package.json package-lock.json $WORKDIR

RUN npm i
ENTRYPOINT [ "npm", "start" ]