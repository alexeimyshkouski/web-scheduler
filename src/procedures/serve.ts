import { server, scheduler } from '../app'
import Pino from 'pino'

interface I_ServeOptions {
    port?: number,
    trace?: boolean,
    file?: string
}

export default function serve(options: I_ServeOptions = {}): void {
    options = Object.assign({
        port: 5005,
        trace: false,
        file: null
    }, options)

    let dest, opts = {}

    if (options.file) {
        dest = Pino.extreme(options.file)
    } else {
        opts = {
            prettyPrint: {
                levelFirst: true,
                translateTime: 'SYS:yyyy-mm-dd HH:MM:ss.l'
            }
        }
        dest = Pino.destination()
    }

    const logger = Pino(opts, dest)

    if (options.trace) {
        logger.level = 'trace'
    }

    function traceEvent(event: string, id: string) {
        logger.trace({ id }, event)
    }

    for (let event of ['schedule', 'expire', 'cancel']) {
        scheduler.on(event, traceEvent.bind(scheduler, event))
    }

    server.listen(options.port, () => {
        logger && logger.info({ port: options.port }, 'listening')
    })
}