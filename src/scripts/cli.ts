import commander from 'commander'
import {
    version
} from '../../package.json'

import serve from '../procedures/serve'

commander.version(version)

commander.command('serve')
    .description('start web service for scheduling')
    .option('-t, --trace', 'enable tracing events')
    .option('-f, --file', 'file to log in')
    .action(serve)

commander.command('set')
    .description('schedule task')
    .option('-t, --timeout <timeout>', 'set timeout in ms', parseInt)
    .action(() => {})

commander.parse(process.argv)