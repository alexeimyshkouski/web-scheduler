// internals
import EventEmitter from 'events'
// essentials
import uuid4 from 'uuid/v4'
import schedule from 'node-schedule'

type Immediate = NodeJS.Immediate

class ImmediateJob {
    protected date: Date
    protected immediateID: Immediate
    protected invoked: boolean
    protected cancelled: boolean

    constructor(cb: () => void) {
        this.date = new Date()
        this.invoked = false
        this.cancelled = false
        this.immediateID = setImmediate(() => {
            this.invoked = true
            cb()
        })
    }

    nextInvocation(): Date {
        return this.date
    }

    cancel(): boolean {
        if(this.invoked) {
            return false
        }

        clearImmediate(this.immediateID)
        return this.cancelled = true
    }
}

type T_Job = schedule.Job | ImmediateJob

interface I_ScheduleOptions {
    timeout?: number,
    date?: number
}

function isNumber(value: any): value is number {
    return (typeof value)[0] === 'n'
}

function isString(value: any): value is string {
    return (typeof value)[0] === 's'
}

type UUIDv4 = string
function isUUIDv4(value: any): value is UUIDv4 {
    // 109156be-c4fb-41ea-b1b4-efe1671c5836
    return isString(value) && /^(?:[\da-f]){8}(-(?:[\da-f]){4}){3}-(?:[\da-f]){12}$/.test(value)
}

function _emit(this: Scheduler, ID: UUIDv4): void {
    this._scheduled.delete(ID)
    setImmediate(this.emit.bind(this, 'expire', ID))
}

class Scheduler extends EventEmitter {
    protected _scheduled: Map<UUIDv4, T_Job>

    constructor() {
        super()
        this._scheduled = new Map()
    }

    get(ID: UUIDv4) {
        return this._scheduled.get(ID)
    }

    has(ID: UUIDv4) {
        return this._scheduled.has(ID)
    }

    schedule(options: I_ScheduleOptions = {}, payload: string = '') {
        const ID: UUIDv4 = uuid4()

        let scheduleOption: Date | null = null
        
        if (isNumber(options.timeout) && options.timeout > 0) {
            scheduleOption = new Date(Date.now() + options.timeout)
        } else if (isNumber(options.date)) {
            scheduleOption = new Date(options.date)
        }
                
        let job: T_Job

        const cb = _emit.bind(this, ID)
        let emit: () => void

        if (scheduleOption) {
            job = schedule.scheduleJob(scheduleOption, cb)
        } else {
            job = new ImmediateJob(cb)
        }

        if(job) {
            this._scheduled.set(ID, job)
            emit = this.emit.bind(this, 'schedule', ID)
        } else {
            emit = this.emit.bind(this, 'cancel', ID)
        }

        setImmediate(emit)

        return ID
    }

    cancel(ID: UUIDv4) {
        if (this._scheduled.has(ID)) {
            const job = this._scheduled.get(ID) as T_Job
            this._scheduled.delete(ID)

            const cancelled = job.cancel()
            
            if(cancelled) {
                setImmediate(this.emit.bind(this, 'cancel', ID))
                return cancelled
            }
        }

        return false
    }
}

export default Scheduler