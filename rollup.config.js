import json from 'rollup-plugin-json'
import typescript from 'rollup-plugin-typescript'

const external = (id, parentId, isResolved) => {
    if (parentId) {
        if (isResolved) {
            // console.log(/node_modules/.test(id), id)
            return /node_modules/.test(id)
        }

        // console.log(/^[^.]/.test(id), id)
        return /^[^.]/.test(id)
    }

    // console.log(false, id)
    return false
}

export default [
    {
        input: ['src/index.ts', 'src/app.ts'],
        output: {
            format: 'esm',
            dir: 'dist/esm',
            entryFileNames: '[name].mjs',
            chunkFileNames: 'chunks/[hash].mjs',
            preferConst: true
        },
        external,
        plugins: [
            typescript()
        ]
    },

    {
        input: ['src/index.ts', 'src/app.ts', 'src/scripts/cli.ts', 'src/scripts/serve.ts'],
        output: {
            format: 'cjs',
            dir: 'dist/cjs',
            entryFileNames: '[name].js',
            chunkFileNames: 'chunks/[hash].js'
        },
        external,
        plugins: [
            typescript(),
            json()
        ]
    }
]
